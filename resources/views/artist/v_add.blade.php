@extends('v_template')

@section('main')
<div class="container mt-5">
  <div class="row">
    <div class="col-lg-4 offset-lg-4">
      <div class="container__box">
        <h1 class="text-center mb-4">Add New Artist</h1>
        <form action="{{ url()->current() }}" method="post" enctype="multipart/form-data">
          @csrf
          <div class="mb-3">
            <label id="album_image" class="form-label">Album Image</label>
            <input type="file" class="form-control" for="album_image" name="album_image" accept="image/*">
          </div>
          <div class="mb-3">
            <label id="album_name" class="form-label">Album Name</label>
            <input type="text" class="form-control" for="album_name" name="album_name" value="{{ Request::post('album_name') }}">
          </div>
          <div class="mb-3">
            <label id="artist_name" class="form-label">Artist Name</label>
            <input type="text" class="form-control" for="artist_name" name="artist_name" value="{{ Request::post('artist_name') }}">
          </div>
          <label id="release_date" class="form-label">Date Release</label>
          <div class="input-group mb-3">
            <input type="text" class="form-control datepicker" for="release_date" name="release_date" aria-describedby="release_date_addon" value="{{ Request::post('release_date') }}">
            <span class="input-group-text" id="release_date_addon"><i class="fa-regular fa-calendar-days"></i></span>
          </div>
          <div class="mb-3">
            <label id="sample_audio" class="form-label">Sample Audio</label>
            <input type="file" class="form-control" for="sample_audio" name="sample_audio" accept=".mp3,audio/*">
          </div>
          <label id="price" class="form-label">Price</label>
          <div class="input-group mb-3">
            <input type="number" class="form-control" for="price" name="price" aria-describedby="price_addon" value="{{ Request::post('price') }}">
            <span class="input-group-text" id="price_addon">円</span>
          </div>
          <div class="text-center">
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection

@section('btmSlot')
<script>
  $('.datepicker').datepicker();
</script>
@endsection