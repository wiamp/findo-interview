@extends('v_template')

@section('main')
<style>
  .table__artist {
    vertical-align: top;
  }

  .table__artist img {
    width: 100px;
  }
</style>
<div class="container mt-5">
  <a href="{{ url()->route('artistAdd') }}" class="btn btn-outline-secondary btn-sm"><i class="fa-solid fa-plus"></i> Add New</a>
  <div class="mb-3"></div>
  @if (session('__status'))
  <div class="alert alert-success" role="alert">
    {{ session('__status') }}
  </div>
  @endif
  @if (!empty($artists))
  <table class="table table__artist">
    <thead>
      <tr>
        <th>Num</th>
        <th>Album Name</th>
        <th>Artist Name</th>
        <th>Date Release</th>
        <th>Sample Audio</th>
        <th>Price</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      @php ( $i = 1 )
      @foreach ($artists as $val)
      <tr>
        <td scope="row">{{ $i }}</td>
        <td>
          <div class="d-flex">
            <img src="{{ url($val->image_url) }}" alt="">
            <div class="ms-1">{{ $val->album_name }}</div>
          </div>
        </td>
        <td>{{ $val->artist_name }}</td>
        <td>{{ date('d M Y', strtotime($val->release_date)) }}</td>
        <td>
          <button type="button" class="btn btn-outline-secondary btn-sm" onclick="playMusic('play-{{ $val->artist_id }}')">
            <i class="fa-solid fa-play"></i>
          </button>
        </td>
        <td>{{ number_format($val->price, 0, '.', ',') }} 円</td>
        <td>
          <a href="{{ url()->route('artistDetail', ['artist_id' => $val->artist_id]) }}" class="btn btn-warning btn-sm"><i class="fa-regular fa-pen-to-square"></i></a>
          <button type="button" class="btn btn-danger btn-sm" data-bs-toggle="modal" data-bs-target="#delete-artist-{{ $val->artist_id }}">
            <i class="fa-regular fa-trash-can"></i>
          </button>
        </td>
      </tr>
      @php ( $i++ )
      @endforeach
    </tbody>
  </table>
  @else
  <div class="alert alert-warning" role="alert">
    <p class="mb-0">Data is unavailable</p>
  </div>
  @endif
</div>

@foreach ($artists as $val)
<!-- Modal Play -->
<div class="modal fade" id="play-{{ $val->artist_id }}" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        <div>
          <audio controls>
            <source src="{{ url($val->sample_url) }}">
            Your browser does not support the audio element.
          </audio>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal Delete -->
<div class="modal fade" id="delete-artist-{{ $val->artist_id }}" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <div class="py-5 text-center">
          Are you sure want to delete this artist?
        </div>
        <div class="clearfix">
          <button type="button" class="btn btn-secondary btn-sm float-start" data-bs-dismiss="modal">Cancel</button>
          <a href="{{ url()->route('artistDelete', ['artist_id' => $val->artist_id]) }}" class="btn btn-danger btn-sm float-end">Delete</a>
        </div>
      </div>
    </div>
  </div>
</div>
@endforeach
@endsection

@section('btmSlot')
<script>
  function playMusic(id) {
    $('#' + id).modal('show');
    $('#' + id).find('audio')[0].play();

    $('#' + id).on('hide.bs.modal', function(e) {
      $('#' + id).find('audio')[0].pause();
    })
  }
</script>
@endsection