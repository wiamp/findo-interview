## Build

PHP 7.4

Laravel 8.8

## Requirement

You need to install PHP client like xampp or laragon

## Instalation

1. Create database "interview_findo" for migrating or you can run SQL file in public folder.

2. Configure .env for database configuration (depend on your environment).

3. Run "composer install" in terminal laravel folder to install Laravel requirement for running.

4. Copy .env.development to .env and configure you database configuration in .env

5. Run "php artisan migrate --seed" for running migration, factory and seed.

6. Run "php artisan storage:link" for linking storage folder with public folder or you can set CDN subdomain that targeting storage public folder.

7. Run "php artisan serve" for run Laravel locally.

8. Open http://localhost:8000 or http://127.0.0.1:8000
