<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'App\Http\Controllers\Artist@index')->name('artist');
Route::any('/artist/add', 'App\Http\Controllers\Artist@add')->name('artistAdd');
Route::any('/artist/detail/{artist_id}', 'App\Http\Controllers\Artist@detail')->name('artistDetail');
Route::get('/artist/delete/{artist_id}', 'App\Http\Controllers\Artist@delete')->name('artistDelete');
