<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use App\Models\Artist as ArtistModel;

/**
 * @author Saka Mahendra Arioka
 *         saka@sakarioka.com
 *         6285338845666
 */
class Artist extends Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function index(Request $request)
  {
    // load model
    $mArtist = new ArtistModel();

    // set data
    $this->data['title']   = $this->data['title'] . ' :: Artist';
    $this->data['artists'] = $mArtist->getAll();

    // set view
    return view('artist.v_index', $this->data);
  }

  public function add(Request $request)
  {
    // load model
    $mArtist = new ArtistModel();

    if ($data = $request->post()) {
      // validate data input
      $errors = $mArtist->validate($data);

      // validate empty image ( required )
      if (empty($request->file('album_image'))) {
        $errors['album_image'] = 'Select album image to upload.';
      }

      if (empty($request->file('sample_audio'))) {
        $errors['sample_audio'] = 'Select sample audio to upload.';
      }

      // check errors
      if (empty($errors)) {
        $imageDir = $request->file('album_image')->store('public/album/' . date('Y') . '/' . date('m'));
        $audioDir = $request->file('sample_audio')->store('public/song/' . date('Y') . '/' . date('m'));

        // insert new artist into database
        DB::insert("
          INSERT INTO artists 
          (artist_name, album_name, image_url, release_date, price, sample_url) 
          VALUES (?, ?, ?, ?, ?, ?)
        ", [
          $data['artist_name'],
          $data['album_name'],
          str_replace('public/', 'storage/', $imageDir),
          date('Y-m-d', strtotime($data['release_date'])),
          $data['price'],
          str_replace('public/', 'storage/', $audioDir)
        ]);

        // redirect to list
        return redirect()->route('artist')->with('__status', 'Artist list updated.');
      }
    }

    // set data
    $this->data['title'] = $this->data['title'] . ' :: Artist :: Add New';

    // set view
    return view('artist.v_add', $this->data);
  }

  public function detail(Request $request, $artist_id)
  {
    // load model
    $mArtist = new ArtistModel();

    // set data
    $this->data['title'] = $this->data['title'] . ' :: Artist :: Detail';
    $this->data['artist'] = ArtistModel::where('artist_id', $artist_id)->first();

    if ($data = $request->post()) {
      // validate data input
      $errors = $mArtist->validate($data);

      // check errors
      if (empty($errors)) {
        if (!empty($request->file('album_image'))) {
          $imageDir = $request->file('album_image')->store('public/album/' . date('Y') . '/' . date('m'));
          $imageDir = str_replace('public/', 'storage/', $imageDir);
        } else {
          $imageDir = $this->data['artist']['image_url'];
        }

        if (!empty($request->file('sample_audio'))) {
          $audioDir = $request->file('sample_audio')->store('public/song/' . date('Y') . '/' . date('m'));
          $audioDir = str_replace('public/', 'storage/', $audioDir);
        } else {
          $audioDir = $this->data['artist']['sample_url'];
        }

        // insert new artist into database
        DB::update("
          UPDATE artists 
          SET artist_name = ?, 
              album_name = ?, 
              image_url = ?, 
              release_date = ?, 
              price = ?, 
              sample_url = ?
          WHERE artist_id = ?
        ", [
          $data['artist_name'],
          $data['album_name'],
          $imageDir,
          date('Y-m-d', strtotime($data['release_date'])),
          $data['price'],
          $audioDir,
          $artist_id
        ]);

        // redirect to list
        return redirect()->route('artist')->with('__status', 'Artist list updated.');
      }
    }

    // set view
    return view('artist.v_detail', $this->data);
  }

  public function delete(Request $request, $artist_id)
  {
    $mArtist = ArtistModel::find($artist_id);
    $mArtist->delete();

    return redirect()->route('artist');
  }
}
