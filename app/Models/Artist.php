<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Support\Facades\DB;

class Artist extends Model
{
  use HasFactory;
  use SoftDeletes;

  // table name
  protected $table = 'artists';

  // primary key
  protected $primaryKey = 'artist_id';

  // validation data
  protected $maxInput = 200;

  // validation
  public function validate($data)
  {
    $errors = [];

    if (empty(trim($data['album_name']))) {
      $errors['album_name'] = 'This field cannot be empty.';
    } elseif (mb_strlen($data['album_name']) > $this->maxInput) {
      $errors['album_name'] = 'Maximum length is '.$this->maxInput.' characters.';
    }

    if (empty(trim($data['artist_name']))) {
      $errors['artist_name'] = 'This field cannot be empty.';
    } elseif (mb_strlen($data['artist_name']) > $this->maxInput) {
      $errors['artist_name'] = 'Maximum length is '.$this->maxInput.' characters.';
    }

    if (empty(trim($data['release_date']))) {
      $errors['release_date'] = 'This field cannot be empty.';
    }

    if (empty(trim($data['price'])) || $data['price'] < 0) {
      $errors['price'] = 'This field cannot be empty.';
    }

    return $errors;
  }

  // get all
  public function getAll()
  {
    $data = DB::table('artists')
              ->where('deleted_at', null)
              ->get();

    return $data;
  }
}
