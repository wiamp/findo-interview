<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ArtistFactory extends Factory
{
  /**
   * Define the model's default state.
   *
   * @return array
   */
  public function definition()
  {
    $imageDir = 'storage/app/public/album/migration';

    if (!file_exists($imageDir)) {
      mkdir($imageDir, 0755, true);
    }

    $image = $this->faker->image($imageDir, 640, 480, null, true, true, null, true);

    return [
      'artist_name' => $this->faker->name(),
      'album_name' => $this->faker->word(),
      'image_url' => str_replace('storage/app/public', 'storage', $image),
      'release_date' => $this->faker->date(),
      'price' => $this->faker->randomNumber(8, true),
      'sample_url' => $this->faker->randomElement(['storage/song/migration/4210.mp3', 'storage/song/migration/4202.mp3'])
    ];
  }
}
